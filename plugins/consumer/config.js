module.exports = {
  elasticUrl: process.env.ELASTIC_SEARCH_URL,
  elasticIndex: process.env.ELASTIC_SEARCH_INDEX,
};
