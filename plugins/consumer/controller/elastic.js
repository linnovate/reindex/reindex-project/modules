const elasticsearch = require("elasticsearch");
const config = require("../config");
const _ = require("lodash");

const client = new elasticsearch.Client({
  host: config.elasticUrl,
  maxRetries: Infinity
});

const indexRecord = (content, callback) => {
  const body = _.omit(content, ["_id"]);
  return new Promise(async resolve => {
    try {
      console.debug("saving to elastic");
      const res = await client.index({
        index: config.elasticIndex,
        id: content._id,
        body: body
      });

      return callback(res);
    } catch (err) {
      return callback(err);
    }
  });
};
module.exports = {
  indexRecord: indexRecord
};
