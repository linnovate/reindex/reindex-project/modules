const request = require("request");
module.exports = function(body, callback) {
  // Send a notification
  console.log(JSON.stringify(body, 0, 2));
  try {
    let formedHeader = { ...body.headers };
    request[body.method](
      { url: body.route, body: body.params, headers: formedHeader },
      (err, response, body) => {
        if (!err) {
          console.log(JSON.stringify(body));
          console.log(JSON.stringify(response, 0, 2));
          return callback(null, {
            code: 201,
            message: "sent message using fallback api",
            success: true,
            data: body
          });
        } else {
          console.error(err);
          return callback(null, {
            code: 500,
            message: err,
            success: false,
            data: null
          });
        }
      }
    );
  } catch (err) {
    return callback(null, { message: "not ok" });
  }
};
