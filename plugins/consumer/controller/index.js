

const elasticCtrl = require('./elastic');

module.exports.elasticIndex = function(ch, msg) {
  elasticCtrl.indexRecord(JSON.parse(msg.content.toString()), (error, output) => {
      const reply = error || output;
    ch.sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify(reply)), {
      correlationId: msg.properties.correlationId
    });
    ch.ack(msg);
  });
};


const fallbackRequest = require("./fallbackRequest");

module.exports.fallbackRequest = function(ch, msg) {
  fallbackRequest(JSON.parse(msg.content.toString()), (error, output) => {
    const reply = error || output;
    ch.sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify(reply)), {
      correlationId: msg.properties.correlationId
    });
    ch.ack(msg);
  });
};
