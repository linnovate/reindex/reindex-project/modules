const restApiDataSource = require("./restApiConnector");

class ConsumerApi {
  constructor(consumerApiUrl) {
    return new restApiDataSource(
      consumerApiUrl,
      true,
      "",
      "",
      [], // KEY:VALUE headers to inject
      "post"
    );
  }
}

module.exports.ConsumerApi = ConsumerApi;

