const elasticsearch = require("elasticsearch");

class ElasticsearchApi {
    constructor(elasticsearchUrl, index) {
        this.client = new elasticsearch.Client({
            host: elasticsearchUrl
        });
        this.index = index
    }
}

module.exports.ElasticsearchApi = ElasticsearchApi;