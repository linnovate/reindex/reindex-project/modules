const restApiDataSource = require("./restApiConnector");

class groupApi {
  constructor(groupApiUrl) {
    return new restApiDataSource(
      groupApiUrl,
      true,
      "Bearer ",
      "",
      [], // KEY:VALUE headers to inject
      "post"
    );
  }
}

module.exports.GroupApi = groupApi;

