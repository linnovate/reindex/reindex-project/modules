const restApiDataSource = require("./restApiConnector");

class ModelApi {
  constructor(modelApiUrl) {
    return new restApiDataSource(
      modelApiUrl,
      true,
      "Bearer ",
      "",
      [], // KEY:VALUE headers to inject
      "post"
    );
  }
}

module.exports.ModelApi = ModelApi;

