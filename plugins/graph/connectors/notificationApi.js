const restApiDataSource = require("./restApiConnector");

class NotificationApi {
    constructor(notificationApiUrl) {
        return new restApiDataSource(
            notificationApiUrl,
            true,
            "",
            "", [], // KEY:VALUE headers to inject
            "post"
        );
    }
}
module.exports.NotificationApi = NotificationApi;