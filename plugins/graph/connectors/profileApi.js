const restApiDataSource = require("./restApiConnector");

class ProfileApi {
  constructor(profileApiUrl) {
    return new restApiDataSource(
      profileApiUrl,
      true,
      "Bearer ",
      "",
      [], // KEY:VALUE headers to inject
      "post"
    );
  }
}

module.exports.ProfileApi = ProfileApi;

