const { RESTDataSource } = require("apollo-datasource-rest");

class RestApiCaller extends RESTDataSource {
  constructor(
    baseurl,
    requiresAuthorization = false,
    authorizationPrefix = "",
    authorizationSuffix = "",
    injectHeaders = [],
    fallbackMethod = "post"
  ) {
    super();
    this.baseURL = baseurl;
    this.requiresAuthorization = requiresAuthorization;
    this.injectHeaders = injectHeaders;
    this.authorizationPrefix = authorizationPrefix;
    this.authorizationSuffix = authorizationSuffix;
    this.fallbackMethod = fallbackMethod;
  }

  async willSendRequest(request) {
    if (request.path.includes("fallbackRequest")) {
      request.body.headers = {
        ...this.injectHeaders,
        Authorization: this.context.token
      };
    }
    if (this.requiresAuthorization) {
      request.headers.set(
        "Authorization",
        `${this.authorizationPrefix}${this.context.token}${this.authorizationSuffix}`
      );
    }
    if (this.injectHeaders.length > 0) {
      this.injectHeaders.map(({ key, value }) => {
        request.headers.set(key, value);
      });
    }
  }
  /*
   * Api SendRequest
   * sends an http request based on method|fallback method to the base url defined for the class instance and route defined in the sendRequest sending
   * @PARAM method: the method type to send (get|post|put|delete)
   * @PARAM route: string route
   * @PARAM params: Object parameters to send
   */
  async sendRequest(method, route, params = {}, noCache = false) {
    return await this[method || this.fallbackMethod](route, params, {
      cache: "no-cache"
    });
  }
}

module.exports = RestApiCaller;
