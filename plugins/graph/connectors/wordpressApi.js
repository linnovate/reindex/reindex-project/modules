const restApiDataSource = require("./restApiConnector");

class wordpressApi {
  constructor(wordpressApiUrl) {
    return new restApiDataSource(
      wordpressApiUrl,
      false,
      "Bearer ",
      "",
      [], // KEY:VALUE headers to inject
      "post"
    );
  }
}

module.exports.WordpressApi = wordpressApi;
