const {
  SchemaDirectiveVisitor,
  AuthenticationError,
  defaultFieldResolver
} = require("apollo-server-express");

/*
 * Checks if user is authenticated
 * If is allowed to acces only its own data
 * If his role is matching the role that protects this resource
 */
class RequireAuthDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    const { resolve = defaultFieldResolver } = field;

    field.resolve = async function(...args) {
      const [result, fieldArguments, context] = args;
      const token = context.token;
        console.log(token)
      // if the resource is data protected but a token hasn't been supplied
      if (!token || token === null)
        throw new AuthenticationError("Resource requires authentication");

      const res = await context.dataSources.profileApi.sendRequest(
        "post",
        "/auth/checkAuthentication",
        {} // Additional Parameters if required
      );
      // if havent find any loggedin user
      if (!res.data._id || !res.data)
        throw new AuthenticationError("You are not autherized");

      context.user = res.data;

      return await resolve.apply(this, args);
    };
  }
}

module.exports.requireAuthDirective = RequireAuthDirective;
