const {
  SchemaDirectiveVisitor,
  AuthenticationError,
} = require("apollo-server-express");
const cookieParser = require("cookie-parser");

const checkWordpressCookies = (ctx) => {
  const cookies = cookieParser.JSONCookies(ctx.cookies);

  let wordpressCookie = false;
  Object.keys(cookies).map((key) => {
    if (key.includes("wordpress_logged_in")) {
      wordpressCookie = cookies[key];
    }
  });

  if (wordpressCookie == false) {
    throw new AuthenticationError("no wordpress login.");
  }

  return wordpressCookie;
};

/**
 * Usage: @wordpressAuth(roles:[string]) to allow query/mutations to apply to many roles
 */
class WordpressAuthDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    const { resolve = defaultFieldResolver } = field;
    const { roles } = this.args;
    field.resolve = async function (...args) {
      const [, params, context] = args;
      //Gets the wordpress cookie from the request and return the wordpress_logged_in cookie value
      let authenticationCookie = checkWordpressCookies(context);
      //checks the wordpress cookie value with the wordpress , the wordpress should return the roles based on the cookie value
      const userRole = await context.dataSources.wordpressApi.sendRequest(
        "post",
        "/v1/users/" + authenticationCookie
      );

      let parsedUserRole = JSON.parse(userRole);

      let boolCanUse = parsedUserRole.every((role) => roles.indexOf(role) > -1);

      if (boolCanUse) {
        const result = await resolve.apply(this, args);
        return result;
      } else {
        throw new AuthenticationError(
          "Cannot Access this resource (wordpress role level dont match)."
        );
      }
    };
  }
}

module.exports.wordpressAuthDirective = WordpressAuthDirective;
