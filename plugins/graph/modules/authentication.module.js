const modelModule = require("../modules/model.module");
const { ApolloError } = require("apollo-server");
const profileModule = require("../modules/profile.module");
const { createBinding } = require("../modules/notification.module");
const phone = require("phone");

//Authentication Module Methods
const createProfile = async (data, context) => {
  console.debug("createProfileModule:" + JSON.stringify(data));
  const { device, appify, ...rest } = data.data;
  const profile = await context.dataSources.profileApi.sendRequest(
    "post",
    "/auth/register",
    { data: { ...rest } }
  );
  console.debug("createProfileModule Results" + JSON.stringify(profile));
  return profile;
};

const checkIfHaveMedicalReportIDOrCreate = async (
  { parent, args, context, info },
  profile
) => {
  console.debug("checkIfHaveMedicalReportIDOrCreate");
  if (profile && profile.medicalReport == null) {
    const medicalReport = await modelModule.createModel(
      parent,
      args,
      context,
      info
    );
    if (medicalReport._id) {
      // put the model ID in profile medical report id
      console.debug("update user with medical report");
      await profileModule.updateProfile(
        context,
        {
          id: profile._id,
          data: {
            medicalReport: medicalReport._id,
          },
        },
        context,
        info
      );
    }
  }
};

const handleNotificationBinding = async (
  parent,
  { device, profile: { _id, phoneNumber, devices, groups } },
  context,
  info
) => {
  const { fcmId, deviceName } = device || { fcmId: false, deviceName: false };
  if (!fcmId || !deviceName) return;
  let exists = false;
  if (devices && devices.length > 0) {
    devices.forEach((device) => {
      if (Object.values(device).indexOf(fcmId) > -1) {
        exists = true;
      }
    });
  }
  if (!exists && fcmId && deviceName && phoneNumber) {
    const pushBinding = await createBinding(
      parent,
      {
        id: _id,
        tags: groups.length > 0 ? groups.map((group) => group._id) : null,
        bindingType: "fcm",
        bindingAddress: fcmId,
      },
      context,
      info
    );
    const smsBinding = await createBinding(
      parent,
      {
        id: _id,
        tags: groups.length > 0 ? groups.map((group) => group._id) : null,
        bindingType: "sms",
        address: phone(phoneNumber, "ISR")[0],
      },
      context,
      info
    );
  }
};

module.exports = {
  handleRegister: async (parent, args, context, info) => {
    // create profile
    const profile = await createProfile(args, context);
    if (profile.data == null) return profile;
    const medicalReportCreation = await checkIfHaveMedicalReportIDOrCreate(
      { parent, args, context, info },
      profile.data
    );
    const handleBinding = await handleNotificationBinding(
      parent,
      {
        device: args.data.device,
        profile: profile.data,
      },
      context,
      info
    );
    await context.dataSources.consumerApi.sendRequest(
      "post",
      "/api/elastic/index",
      profile.data
    );

    return profile;
  },
  handleLogin: async (parent, args, context, info) => {
    const { device, ...rest } = args.data;
    const profile = await context.dataSources.profileApi.sendRequest(
      "post",
      "/auth/login",
      { data: { ...rest } }
    );
    const medicalReportCreationIfMissing = await checkIfHaveMedicalReportIDOrCreate(
      { parent, args, context, info },
      profile.data
    );

    const handleBinding = await handleNotificationBinding(
      parent,
      {
        device: args.data.device,
        profile: profile.data,
      },
      context,
      info
    );
    await context.dataSources.consumerApi.sendRequest(
      "post",
      "/api/elastic/index",
      profile.data
    );
    return profile;
  },
  handleJoin: async (parent, args, context, info) => {
    const profile = await context.dataSources.profileApi.sendRequest(
      "post",
      "/auth/join",
      args
    );
    const medicalReportCreationIfMissing = await checkIfHaveMedicalReportIDOrCreate(
      { parent, args, context, info },
      profile.data
    );
    const handleBinding = await handleNotificationBinding(
      parent,
      {
        device: args.data.device,
        profile: profile.data,
      },
      context,
      info
    );
    await context.dataSources.consumerApi.sendRequest(
      "post",
      "/api/elastic/index",
      profile.data
    );
    return profile;
  },
};
