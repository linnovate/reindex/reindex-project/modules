const { getProfiles } = require("./profile.module");
const queryBuilder = require("./queryBuilder.js");

module.exports = {
  async putSettings(context) {
    await context.dataSources.elasticsearchApi.client.indices.close({
      index: context.dataSources.elasticsearchApi.index,
    });
    const res = await context.dataSources.elasticsearchApi.client.indices.putSettings(
      {
        index: context.dataSources.elasticsearchApi.index,
        body: require("./elastic.settings.json"),
      }
    );
    await context.dataSources.elasticsearchApi.client.indices.open({
      index: context.dataSources.elasticsearchApi.index,
    });
    return res;
  },
  async putMapping(context) {
    const res = await context.dataSources.elasticsearchApi.client.indices.putMapping(
      {
        index: context.dataSources.elasticsearchApi.index,
        type: "_doc",
        body: require("./elastic.mapping.json"),
      }
    );
    return res;
  },
  async createIndex(context) {
    const res = await context.dataSources.elasticsearchApi.client.indices.create(
      {
        index: context.dataSources.elasticsearchApi.index,
      }
    );
    return res;
  },
  async deleteIndex(context) {
    console.log(context.dataSources.elasticsearchApi);
    const res = await context.dataSources.elasticsearchApi.client.indices.delete(
      {
        index: context.dataSources.elasticsearchApi.index,
      }
    );
    return res;
  },

  async makebulk(context) {
    try {
      let profileRes = await getProfiles(null, { ids: [] }, context);
      const bulk = [];
      for (const current in profileRes) {
        const body = Object.keys(profileRes[current]).reduce((object, key) => {
          if (key !== "_id") {
            object[key] = profileRes[current][key];
          }
          return object;
        }, {});
        bulk.push(
          {
            index: {
              _index: context.dataSources.elasticsearchApi.index,
              _id: profileRes[current]._id,
            },
          },
          body
        );
      }
      return bulk;
    } catch (err) {
      console.log("eee", err);
    }
  },
  async indexAll(context, bulk) {
    const res = await context.dataSources.elasticsearchApi.client.bulk({
      index: context.dataSources.elasticsearchApi.index,
      type: "_doc",
      maxRetries: 5,
      body: bulk,
    });
    return res;
  },
  async searchElastic(parent, { query }, context, info) {
    const body = await queryBuilder.queryBuilder(query);
    const response = await context.dataSources.elasticsearchApi.client.search({
      index: context.dataSources.elasticsearchApi.index,
      size: query.size || 10,
      from: query.from || 0,
      body,
    });

    return response.hits;
  },
};
