exports.createGroup = async(parent, args, context, info) => {
    const groupCreationResult = await context.dataSources.groupApi.sendRequest(
        "post",
        "/api/group/new", { id: context.user._id }
    );
    const { code, message, sucess, data } = groupCreationResult;
    return data;
};
exports.getGroup = async(parent, { id }, context, info) => {
    const groupFetchResult = await context.dataSources.groupApi.sendRequest(
        "get",
        "/api/group/" + id
    );
    const { code, message, sucess, data } = groupFetchResult;
    return data;
};
exports.getGroups = async(parent, { ids }, context, info) => {
    const groupsFetchResult = await context.dataSources.groupApi.sendRequest(
        "post",
        "/api/group", { ids: ids }
    );
    const { code, message, sucess, data } = groupsFetchResult;
    return data;
};
exports.updateGroup = async(parent, { id, data }, context, info) => {
    const groupUpdateResult = await context.dataSources.groupApi.sendRequest(
        "put",
        "/api/group/" + id, { data }
    );
    await context.dataSources.consumerApi.sendRequest(
        "post",
        "/api/elastic/index",
        Object.assign({}, data, { id })
    )
    const { code, message, sucess } = groupUpdateResult;
    return groupUpdateResult.data;
};
exports.updateGroups = async(parent, { ids, data }, context, info) => {
    const groupsUpdateResult = await context.dataSources.groupApi.sendRequest(
        "put",
        "/api/group", { ids, data }
    );
    const { code, message, sucess } = groupsUpdateResult;
    return groupsUpdateResult.data;
};
exports.deleteGroup = async(parent, { id }, context, info) => {
    const deleteGroupResult = await context.dataSources.groupApi.sendRequest(
        "delete",
        "/api/group/" + id
    );
    const { code, message, sucess, data } = deleteGroupResult;
    return data;
};
exports.deleteGroups = async(parent, { ids }, context, info) => {
    const deleteGroupsResult = await context.dataSources.groupApi.sendRequest(
        "delete",
        "/api/group", { ids }
    );
    const { code, message, sucess, data } = deleteGroupsResult;
    return data;
};
exports.acceptGroup = async(parent, { id, memberId }, context, info) => {
    const acceptGroupResult = await context.dataSources.groupApi.sendRequest(
        "post",
        "/api/group/" + id + "/accept", { memberId }
    );
    const { code, message, sucess, data } = acceptGroupResult;
    return data;
};
exports.addMemberToGroup = async(parent, { id, data }, context, info) => {
    const addMemberToGroupResult = await context.dataSources.groupApi.sendRequest(
        "put",
        "/api/group/" + id + "/add", { data }
    );
    const { code, message, sucess } = addMemberToGroupResult;
    return addMemberToGroupResult.data;
};