const { getProfileByPhoneNumber, updateProfile } = require("./profile.module");
const { handleRegister } = require("./authentication.module");
const { createGroup, getGroup, addMemberToGroup } = require("./group.module");
/*
 * Invite user
 * 1. phoneNumber
 * 2. groupId
 *
 * if no phoneNumber throw
 *
 * if no groupid check owner groups if one group use it (no need to readd owner), if more then one throw provide specific group
 *
 * if no groups at all create (creation adds owner to the group)
 *
 * use the output group
 *
 * if user already exists invite him to the selected group
 *
 * if user doesnt exist create and add to group both approved:false
 *
 *
 */

// validate user phoneNumber
const validateUserPhoneNumber = async (
  parent,
  { phoneNumber },
  context,
  info
) => {
  if (!phoneNumber || phoneNumber === null || phoneNumber === undefined)
    throw new Error("You must provide phoneNumber to invite");
  const profile = await getProfileByPhoneNumber(
    parent,
    { phoneNumber },
    context,
    info
  );
  if (profile) return profile;
  return null;
};

// checks if user has one group or not
const checkIfUserHasOneGroup = async (parent, {}, context, info) => {
  const profile = context.user;
  if (profile.groups.length == 1) {
    return {
      result: true,
      group: profile.groups[0],
    };
  } else if (profile.groups.length > 1) {
    throw new Error("User has many groups please select one");
  } else {
    return { result: false, group: null };
  }
};

// checks if group exists or not and return it
const validateGroup = async (parent, { groupId }, context, info) => {
  const group = await getGroup(parent, { id: groupId }, context, info);
  return group;
};

// validate the groupid if passed, if not passed checks if user already has groups, if he have one group select that if 0 creates if have multiple requests to pass single groupid selection
const groupCreationManager = async (parent, { groupId }, context, info) => {
  let group = null;
  if (!groupId || groupId === null || groupId === undefined) {
    let validation = await checkIfUserHasOneGroup(parent, {}, context, info);
    group = validation.group;
    if (validation.result === false) {
      group = await createGroup(parent, {}, context, info);
    }
  } else {
    group = await validateGroup(parent, { id: groupId }, context, info).group;
  }

  return group;
};

const registerInviteUser = async (parent, { phoneNumber }, context, info) => {
  const registered = await handleRegister(
    parent,
    { data: { phoneNumber } },
    context,
    info
  );
  if (registered.data == null) throw new Error("couldnt register user");
  return registered.data;
};

/*
 * Check groups , if one use if none create if provided groupid use if too many groups error please provide one
 * Check user , add if exists , create if not
 *
 *
 */
exports.makeInvite = async (
  parent,
  { data: { phoneNumber, groupId } },
  context,
  info
) => {
  let group = await groupCreationManager(parent, { groupId }, context, info);
  console.debug("using the following group:" + JSON.stringify(group));
  let profile = await validateUserPhoneNumber(
    parent,
    { phoneNumber },
    context,
    info
  );

  if (profile == null)
    profile = await registerInviteUser(
      parent,
      { phoneNumber: phoneNumber },
      context,
      info
    );

  if (
    !context.user.groups.map((group) => group._id).includes(group._id) &&
    group
  )
    await updateProfile(
      parent,
      {
        id: context.user._id,
        data: {
          groups: [...context.user.groups.map((group) => group_.id), group._id],
        },
      },
      context,
      info
    );

  if (!profile.groups.map((group) => group._id).includes(group._id) && group)
    await updateProfile(
      parent,
      {
        id: profile._id,
        data: {
          groups: [...profile.groups.map((group) => group._id), group._id],
        },
      },
      context,
      info
    );

  if (group)
    await addMemberToGroup(
      parent,
      {
        id: group._id,
        data: { memberId: profile._id, approved: false, role: "Member" },
      },
      context,
      info
    );

  return { id: profile._id, code: profile.activation.activationCode };
};
