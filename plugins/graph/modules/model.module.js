const createModel = async(parent, args, context, info) => {
    const createModelResult = await context.dataSources.modelApi.sendRequest(
        "post",
        "/api/model"
    );

    const { data, success, message, code } = createModelResult;
    return data;
};

const getModel = async(parent, { id }, context, info) => {
    const getModelResult = await context.dataSources.modelApi.sendRequest(
        "get",
        "/api/model/" + id
    );
    const { data, success, message, code } = getModelResult;
    return data;
};

const getModels = async(parent, { ids }, context, info) => {
    const getModelsResult = await context.dataSources.modelApi.sendRequest(
        "post",
        "/api/model", { ids }
    );
    const { data, success, message, code } = getModelsResult;
    return data;
};

const updateModel = async(parent, { id, data }, context, info) => {
    const updateModelResult = await context.dataSources.modelApi.sendRequest(
        "put",
        "/api/model/" + id, { data }
    );
    await context.dataSources.consumerApi.sendRequest(
        "post",
        "/api/elastic/index",
        Object.assign({}, data, { id })
    )
    const { success, message, code } = updateModelResult;
    return updateModelResult.data;
};

const updateModels = async(parent, { ids, data }, context, info) => {
    const updateModelsResult = await context.dataSources.modelApi.sendRequest(
        "put",
        "/api/model/bulk", { ids, data }
    );

    const { success, message, code } = updateModelsResult;
    return updateModelsResult.data;
};

const deleteModel = async(parent, { id }, context, info) => {
    const deleteModelResult = await context.dataSources.modelApi.sendRequest(
        "delete",
        "/api/model" + id
    );

    const { data, success, message, code } = deleteModelResult;
    return data;
};

const deleteModels = async(parent, { ids }, context, info) => {
    const deleteModelsResult = await context.dataSources.modelApi.sendRequest(
        "delete",
        "/api/model/bulk", { ids }
    );

    const { data, success, message, code } = deleteModelsResult;
    return data;
};
module.exports = {
    createModel: createModel,
    getModel: getModel,
    getModels: getModels,
    updateModel: updateModel,
    updateModels: updateModels,
    deleteModel: deleteModel,
    deleteModels: deleteModels
};