exports.createBinding = async (
  parent,
  { identity, id, tags, bindingType, bindingAddress, address },
  context,
  info
) => {
  let res = await context.dataSources.notificationApi.sendRequest(
    "post",
    "/api/binding",
    {
      tag: tags,
      identity: id || identity,
      bindingType: bindingType,
      address: bindingAddress || address
    }
  );
  const { code, message, success, data } = res;
  return res;
};

exports.sendNotification = async (
  parent,
  { identity, tags, body, priority, ttl, title, sound, action, custom },
  context,
  info
) => {
  console.log(identity);
  let res = await context.dataSources.notificationApi.sendRequest(
    "post",
    "/api/notification",
    {
      identity,
      tag: tags,
      body,
      priority,
      ttl,
      title,
      sound,
      action,
      ...custom
    }
  );
  const { code, message, success, data } = res;
  return res;
};
