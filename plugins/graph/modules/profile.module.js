const modelModule = require("./model.module");

const getProfile = async (parent, { id }, context, info) => {
  const getProfileResult = await context.dataSources.profileApi.sendRequest(
    "get",
    "/api/profile",
    { id }
  );
  const { data, message, success, code } = getProfileResult;
  return data;
};

const getProfileByPhoneNumber = async (
  parent,
  { phoneNumber },
  context,
  info
) => {
  const getProfileByPhoneNumberResult = await context.dataSources.profileApi.sendRequest(
    "get",
    "/api/profile",
    { phoneNumber }
  );
  const { data, message, code, success } = getProfileByPhoneNumberResult;
  return data;
};

const getProfiles = async (parent, { ids }, context, info) => {
  const getProfilesResult = await context.dataSources.profileApi.sendRequest(
    "post",
    "/api/profile",
    { ids }
  );
  const { data, message, code, success } = getProfilesResult;
  return data;
};

const updateProfile = async (parent, { id, data }, context, info) => {
  const updateProfileResult = await context.dataSources.profileApi.sendRequest(
    "put",
    "/api/profile",
    {
      id: id,
      data: data,
    }
  );
  await context.dataSources.consumerApi.sendRequest(
    "post",
    "/api/elastic/index",
    Object.assign({}, data, { id })
  );
  const { message, code, success } = updateProfileResult;
  return updateProfileResult.data;
};

const updateProfiles = async (parent, { ids, data }, context, info) => {
  const updatedProfilesResult = await context.dataSources.profileApi.sendRequest(
    "put",
    "/api/profile/bulk",
    { ids: ids, data: data }
  );
  const { code, message, success } = updatedProfilesResult;
  return updatedProfilesResult.data;
};

const deleteProfile = async (parent, { id }, context, info) => {
  // get profile pre deletion
  const profileToDelete = await getProfile(context, id);
  // get the medical report id
  const medicalReportIdToDelete = profileToDelete.medicalReportID;
  // delete the profile
  const deletedProfileResult = await context.dataSources.profileApi.sendRequest(
    "delete",
    "/api/profile",
    { id }
  );

  // delete the medical report
  await modelModule.deleteModel(
    parent,
    { id: medicalReportIdToDelete },
    context,
    info
  );
  const { code, message, success, data } = deletedProfileResult;
  return data;
};

const deleteProfiles = async (parent, { ids }, context, info) => {
  //get profiles pre deleteion for medical report ids
  const profilesToDelete = await getProfiles(parent, { ids }, context, info);
  if (profilesToDelete && profilesToDelete.length > 0) {
    //if found profiles
    const modelIds = [];
    //get thier medical report ids
    profilesToDelete.forEach((profile) => {
      modelIds.push(profile.medicalReportID);
    });
    // delete the medical reports
    const deletedModels = await modelModule.deleteModels(
      parent,
      { modelIds },
      context,
      info
    );
  }

  //finally delete the profiles
  const deletedProfiles = await context.dataSources.profileApi.sendRequest(
    "delete",
    "/api/profile/bulk",
    { ids }
  );
  const { data, message, code, success } = deletedProfiles;
  return data;
};

module.exports = {
  myProfile: async (parent, args, context, info) => {
    return context.user;
  },
  getProfile: getProfile,
  getProfileByPhoneNumber: getProfileByPhoneNumber,
  getProfiles: getProfiles,
  updateProfile: updateProfile,
  updateProfiles: updateProfiles,
  deleteProfile: deleteProfile,
  deleteProfiles: deleteProfiles,
};
