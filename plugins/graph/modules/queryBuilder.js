const async = require("async");
const remove = require("lodash/remove");

const buildRangeObj = (value) => {
  const rangeObj = {};
  if (value.min) {
    rangeObj.gte = value.min;
  }
  if (value.max) {
    rangeObj.lte = value.max;
  }
  return rangeObj;
};
const functions = {
  nameQuery: (body, value, callback) => {
    body.query.bool.must.push({
      dis_max: {
        queries: [
          {
            multi_match: {
              query: value.replace(/\?/g, '"?"'),
              operator: "and",
              type: "phrase",
              slop: 2,
              minimum_should_match: "75%",
              lenient: true,
              fields: ["firstName", "lastName"],
            },
          },
        ],
      },
    });
    return callback(body);
  },
  roleQuery: (body, value, callback) => {
    body.query.bool.must.push({
      dis_max: {
        queries: [
          {
            match: {
              userRole: {
                query: value,
                operator: "and",
                boost: 3,
              },
            },
          },
        ],
      },
    });
    return callback(body);
  },
};

module.exports = {
  queryBuilder(filters) {
    return new Promise((resolve) => {
      const body = {
        query: {
          bool: {
            must: [],
            filter: {
              bool: {
                should: [],
                must: [],
              },
            },
          },
        },
        sort: [],
      };
      const keys = Object.keys(filters);
      async.each(
        keys,
        (key, callback) => {
          if (!functions[`${key}Query`]) {
            return callback(null);
          } else {
            functions[`${key}Query`](body, filters[key], callback);
          }
        },
        (err, result) => {
          return resolve(body);
        }
      );
    });
  },
};
