const {
  handleRegister,
  handleLogin,
} = require("../modules/authentication.module");
const { updateProfile } = require("../modules/profile.module");
const notificationModule = require("../modules/notification.module");
const phone = require("phone");

module.exports = {
  Mutation: {
    loginUser: async (parent, args, context, info) => {
      console.debug("loginUserResolver:" + JSON.stringify(args));
      const login = await handleLogin(parent, args, context, info);
      if (login && login.token)
        context.res.cookie(
          process.env.AUTHENTICATION_COOKIE_NAME,
          login.token,
          {
            maxAge: 9999999,
            httpOnly: true,
          }
        );
      return login;
    },
    registerUser: async (parent, args, context, info) => {
      console.debug("registerUserResolver:" + JSON.stringify(args));
      const profile = await handleRegister(
        parent,
        { data: { ...args.data, activation: { isActive: true } } },
        context,
        info
      );
      const { data } = profile;

      console.debug("registerUserResolver results:" + JSON.stringify(profile));
      if (profile && profile.token)
        context.res.cookie(
          process.env.AUTHENTICATION_COOKIE_NAME,
          login.token,
          {
            maxAge: 9999999,
            httpOnly: true,
          }
        );
      return profile;
    },
    logout: async (parent, args, context, info) => {
      context.res.cookie(process.env.AUTHENTICATION_COOKIE_NAME, "", {
        maxAge: 0,
        httpOnly: true,
      });
        return true;
    },
    validateUser: async (parent, args, context, info) => {
      console.debug("validateUserResolver:" + JSON.stringify(context.user));
      if (context.user) {
        return true;
      }
      return false;
    },
  },
};
