const {
  myProfile,
  getProfile,
  getMyFamily,
  getProfiles,
  updateProfile,
  updateProfiles,
  deleteProfile,
  deleteProfiles,
  search
} = require("../modules/profile.module");

const { getModel } = require("../modules/model.module");

const { getGroups } = require("../modules/group.module");

const {
  deleteIndex,
  createIndex,
  putSettings,
  putMapping,
  makebulk,
  indexAll,
  searchElastic
} = require("../modules/elastic.module");
module.exports = {

  Query: {

    search: async(parent, args, context, info) => {
      const response =  await searchElastic(parent, args, context, info)
      const data = response && response.hits && response.hits.map(a => Object.assign(a._source, { _id: a._id }));
      return {
          profiles: data,
          total: response && response.total,
      }
    }
  },
  Mutation: {
      reindex: async(parent, args, context, info) => {
            await deleteIndex(context);
            await createIndex(context);
            await putSettings(context);
            await putMapping(context);
            const bulk = await makebulk(context);
            if (bulk.length === 0) {
                return 'no records'
            }
            const res = await indexAll(context, bulk)
            return JSON.stringify(res);
      }
  }
};
