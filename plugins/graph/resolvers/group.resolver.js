const {
  createGroup,
  getGroup,
  getGroups,
  updateGroup,
  updateGroups,
  deleteGroup,
  deleteGroups
} = require("../modules/group.module");

const { getProfile } = require("../modules/profile.module");
module.exports = {
  groupMember: {
    profile: async (parent, args, context, info) => {
      const getProfileResult = await getProfile(
        parent,
        { id: parent.memberId },
        context,
        info
      );
      return getProfileResult;
    }
  },
  Query: {
    getMyGroups: async (parent, args, context, info) => {
      const groups = await getGroups(
        parent,
        { ids: context.user.groups },
        context,
        info
      );
      const { data } = groups;
      return data;
    },
    getGroup: async (parent, { id }, context, info) => {
      const group = await getGroup(parent, { id }, context, info);
      const { data } = group;
      return data;
    },
    getGroups: async (parent, { ids }, context, info) => {
      const groups = await getGroups(parent, { ids }, context, info);
      console.log(JSON.stringify(groups));
      const { data } = groups;
      return data;
    }
  },
  Mutation: {
    createGroup: async (parent, args, context, info) => {
      return await createGroup(parent, args, context, info);
    },
    acceptGroup: async (parent, { groupId }, context, info) => {
      return await updateGroup(
        parent,
        {
          groupId,
          data: { members: [{ memberId: context.user._id, approved: true }] }
        },
        context,
        info
      );
    },
    updateGroup: async (parent, { groupId, data }, context, info) => {
      return await updateGroup(parent, { id: groupId, data }, context, info);
    },
    updateGroups: async (parent, { groupIds, data }, context, info) => {
      return await updateGroups(parent, { ids: groupIds, data }, context, info);
    },
    deleteGroup: async (parent, { groupId }, context, info) => {
      return await deleteGroup(parent, { id: groupId }, context, info);
    },
    deleteGroups: async (parent, { groupIds }, context, info) => {
      return await deleteGroups(parent, { ids: groupIds }, context, info);
    }
  }
};
