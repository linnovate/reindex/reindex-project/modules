const { makeInvite } = require("../modules/invite.module");
const { getProfile, updateProfile } = require("../modules/profile.module");
const { handleJoin } = require("../modules/authentication.module");
const notificationModule = require("../modules/notification.module");
const phone = require("phone");
module.exports = {
  Mutation: {
    invite: async (parent, { data }, context, info) => {
      return await makeInvite(parent, { data }, context, info);
    },
    join: async (parent, { id, data }, context, info) => {
      const joinData = await handleJoin(parent, { id, data }, context, info);
      const { code, message, token } = joinData;

      if (joinData && joinData.data != null && joinData.data != undefined)
        await notificationModule.createBinding(
          parent,
          {
            id: joinData.data._id,
            tags: joinData.data.groups,
            bindingType: joinData.data.bindingType || "sms",
            bindingAddress:
              joinData.data.bindingAddress ||
              phone(joinData.data.phoneNumber, "IL")[0],
          },
          context,
          info
        );
      return joinData;
    },
    validateActivationCode: async (parent, { id, code }, context, info) => {
      const user = await getProfile(parent, { id }, context, info);
      return user.activation.activationCode === code ? true : false;
    },
  },
};
