const {
  createModel,
  getModel,
  getModels,
  updateModel,
  updateModels,
  deleteModel,
  deleteModels
} = require("../modules/model.module");
module.exports = {
  Query: {
    getMyMedicalReport: async (parent, args, context, info) => {
      const myMedicalReportID = context.user.medicalReportID;
      const medicalReport = await getModel(
        parent,
        { id: myMedicalReportID },
        context,
        info
      );

      return medicalReport.data;
    },
    getFamilyMemberMedicalReport: async (parent, { id }, context, info) => {
      const medicalReport = await getModel(parent, { id }, context, info);
      return medicalReport.data;
    },
    getMedicalReport: async (parent, { id }, context, info) => {
      const medicalReport = await getModel(parent, { id }, context, info);
      return medicalReport.data;
    },
    getMedicalReports: async (parent, { ids }, context, info) => {
      const medicalReports = await getModels(parent, { ids }, context, info);
      return medicalReports.data;
    }
  },
  Mutation: {
    updateMyMedicalReport: async (parent, { data }, context, info) => {
      const myMedicalReportID = context.user.medicalReportID;
      const medicalReport = await updateModel(
        parent,
        { id: myMedicalReportID, data: data },
        context,
        info
      );

      return medicalReport;
    },
    updateFamilyMemberMedicalReport: async (
      parent,
      { id, data },
      context,
      info
    ) => {
      const medicalReport = await updateModel(
        parent,
        { id: id, data: data },
        context,
        info
      );

      return medicalReport;
    },
    updateMedicalReport: async (parent, { id, data }, context, info) => {
      const medicalReport = await updateModel(
        parent,
        { id: id, data: data },
        context,
        info
      );

      return medicalReport;
    },
    updateMedicalReports: async (parent, { ids, data }, context, info) => {
      const medicalReport = await updateModels(
        parent,
        { ids: ids, data: data },
        context,
        info
      );

      return medicalReport;
    },
    deleteMedicalReport: async (parent, { id }, context, info) => {
      const deleteResult = await deleteModel(parent, { id: id }, context, info);

      return deleteResult;
    },
    deleteMyMedicalReport: async (parent, args, context, info) => {
      const myMedicalReportID = context.user._id;
      const deleteResult = await deleteModel(
        parent,
        { id: myMedicalReportID },
        context,
        info
      );

      return deleteResult;
    },
    deleteMedicalReports: async (parent, { ids }, context, info) => {
      const deleteResult = await deleteModels(parent, { ids }, context, info);

      return deleteResult;
    }
  }
};
