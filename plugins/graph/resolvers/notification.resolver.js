const {
  sendNotification,
  createBinding
} = require("../modules/notification.module");

module.exports = {
  Mutation: {
    sendNotification: async (parent, args, context, info) => {
      return await sendNotification(parent, args.data, context, info);
    },
    createBinding: async (parent, args, context, info) => {
      return await createBinding(parent, args.data, context, info);
    }
  }
};

