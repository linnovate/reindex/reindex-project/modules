const {
  myProfile,
  getProfile,
  getMyFamily,
  getProfiles,
  updateProfile,
  updateProfiles,
  deleteProfile,
  deleteProfiles
} = require("../modules/profile.module");

const { getModel } = require("../modules/model.module");

const { getGroups } = require("../modules/group.module");

module.exports = {
  Query: {
    myProfile: async (parent, args, context, info) => {
      return await myProfile(parent, args, context, info);
    },
    getProfile: async (parent, args, context, info) => {
      return await getProfile(parent, args, context, info);
    },
    getProfileByPhoneNumber: async (parent, args, context, info) => {
      return await getProfileByPhoneNumber(parent, args, context, info);
    },
    getProfiles: async (parent, args, context, info) => {
      return await getProfiles(parent, args, context, info);
    }
  },
  Mutation: {
    updateProfile: async (parent, args, context, info) => {
      return await updateProfile(parent, args, context, info);
    },
    updateProfiles: async (parent, args, context, info) => {
      return await updateProfiles(parent, args, context, info);
    },
    updateFamilyMember: async (parent, args, context, info) => {
      return await updateProfile(parent, args, context, info);
    },
    deleteProfile: async (parent, args, context, info) => {
      return await deleteProfile(parent, args, context, info);
    },
    deleteProfiles: async (parent, args, context, info) => {
      return await deleteProfiles(parent, args, context, info);
    }
  }
};
