module.exports = {
  mutationResponse: {
    __resolveType(response, context, info) {
      if (response.profile) {
        return "profile";
      }
      if (response.medicalReport) {
        return "medicalReport";
      }

      return null;
    }
  }
};
