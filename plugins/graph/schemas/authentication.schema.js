module.exports = `
	input loginInput {
		phoneNumber: String!
		password: String!
		device: deviceInput
		appify: appifyInput
	}

	input deviceInput {
		deviceName:String
		fcmId: String
	}
	input appifyInput {
		installed:Boolean!
	}

	input registerInput {
		firstName: String!
		lastName: String!
		password: String!
		email: String!
		phoneNumber: String!
		device: deviceInput
		appify: appifyInput
	}


	type authenticationResponse {
		token: String
		message: String
	}
	
	type userValidation {
		loggedIn: Boolean
	}

	extend type Mutation {
		loginUser(data: loginInput): authenticationResponse
		registerUser(data: registerInput): authenticationResponse
        logout: Boolean
		validateUser: userValidation @requireAuth
	}
`;

