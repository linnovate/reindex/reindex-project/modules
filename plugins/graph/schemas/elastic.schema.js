module.exports = `
    type SearchReply {
      profiles: [profile]
      total: Int
    }
	extend type Query {
		search(query: JSON): SearchReply
	}

	extend type Mutation {
        reindex: String
	}
`;
