module.exports = `
	type groupMutationResponse implements mutationResponse {
		code: String!
		success: Boolean!
		message: String!
		data: group
	}

	type groupMember {
		profile: profile!
		role: accessLevels!
		approved: Boolean!
	}

	input groupMemberInput {
		memberId: String!
		role: accessLevels
		approved: Boolean!
	}

	enum accessLevels {
		Owner
		Manager
		Member
		LimitedMember
	}

	type group {
		_id: String!
		label: String!
		members: [groupMember]
		createdDate: String!
	}

	input groupInput {
		label: String
		members: [groupMemberInput]
	}

	extend type Query {
		getMyGroups: [group] @requireAuth
		getGroup(id: String!): group @requireAuth
		getGroups(ids: [String]!): [group] @requireAuth
	}

	extend type Mutation {
		createGroup: group! @requireAuth
		acceptGroup(groupId: String!): Boolean @requireAuth
		updateGroup(groupId: String!, data: groupInput!): docMutationResponse! @requireAuth
		updateGroups(groupIds: [String]!, data: groupInput!): docMutationResponse! @requireAuth
		deleteGroup(groupId: String!): docMutationResponse @requireAuth
		deleteGroups(groupIds: [String]!): docMutationResponse @requireAuth
	}

`;
