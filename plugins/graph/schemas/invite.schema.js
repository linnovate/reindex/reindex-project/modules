module.exports = `

	input joinInput {
		password: String!
		email: String!
		code: String!
		appify: appifyInput
	}

	input inviteInput {
		phoneNumber: String!
        firstName: String!
        lastName: String!
		groupId: String
	}

	type inviteResponse {
		id: String,
		code: String
	}


	extend type Mutation {
		invite(data: inviteInput!): inviteResponse! @requireAuth
		join(id: String!,data: joinInput!): authenticationResponse
		validateActivationCode(id: String,code: String): Boolean
	}
`;
