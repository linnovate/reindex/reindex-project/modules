module.exports = `
	type medicalEvent {
		date: String
		description: String
	}

	type sensitivity {
		cause: String
		effect: String
		severity: Int
	}

	type frequency {
		dateFormat: String
		interval: Int
	}

	type medication {
		name: String
		frequency: frequency
	}

	input medicalEventInput {
		date: String
		description: String
	}

	input sensitivityInput {
		cause: String
		effect: String
		severity: Int
	}

	input frequencyInput {
		dateFormat: String
		interval: Int
	}

	input medicationInput {
		name: String
		frequency: frequencyInput
	}
	enum bloodTypes {
		ONEGATIVE
		OPOSITIVE
		ANEGATIVE
		APOSITIVE
		BNEGATIVE
		BPOSITIVE
		ABNEGATIVE
		ABPOSITIVE
	}

	type medicalReport {
		_id: String!
		overallMedicalCondition: String
		significantMedicalEvents: [medicalEvent]
		sensitivities: [sensitivity]
		regularMedications: [medication]
		bloodType: bloodTypes
	}

	input medicalReportInput {
		overallMedicalCondition: String
		significantMedicalEvents: [medicalEventInput]
		sensitivities: [sensitivityInput]
		regularMedications: [medicationInput]
		bloodType: bloodTypes
	}

	extend type Query {
		getMyMedicalReport: medicalReport! @requireAuth	
		getFamilyMemberMedicalReport(id: String!): medicalReport! @requireAuth
		getMedicalReport(id: String!): medicalReport! @requireAuth
		getMedicalReports(ids: [String]!): [medicalReport]! @requireAuth
	}

	extend type Mutation {
		updateMyMedicalReport(data: medicalReportInput): medicalReport! @requireAuth
		updateFamilyMemberMedicalReport(id: String!,data: medicalReportInput!) : medicalReport! @requireAuth
		updateMedicalReport(id: String!, data: medicalReportInput!): medicalReport! @requireAuth
		updateMedicalReports(ids: [String]!, data: medicalReportInput!): docMutationResponse! @requireAuth
		deleteMedicalReport(id: String!): docMutationResponse! @requireAuth
		deleteMyMedicalReport: docMutationResponse! @requireAuth
		deleteMedicalReports(ids: [String]!): docMutationResponse! @requireAuth
	}

`;
