module.exports = `
	type notificationsMutationResponse implements mutationResponse {
		code: String!
		success: Boolean!
		message: String!
	}

	enum priority {
		low
		high
	}

	input notificationInput{
		identity: String!
		tags: [String]
		body: String
		priority: priority
		ttl: Int
		title: String
		sound: String
		action: String
		custom: JSON
	}

	enum bindingTypes {
		apn
		fcm
		gcm
		sms
		facebook
	}

	input bindingInput{
		identity: String!
		tags: [String]
		bindingType: bindingTypes!
		address: String!
	}

	extend type Mutation {
		createBinding(data:bindingInput!): notificationsMutationResponse
		sendNotification(data: notificationInput!): notificationsMutationResponse
	}
`;

