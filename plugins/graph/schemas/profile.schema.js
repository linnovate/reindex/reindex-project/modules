module.exports = `
	interface mutationResponse {
		code: String
		success: Boolean
		message: String
	}

	type userMutationResponse implements mutationResponse {
		code: String
		success: Boolean
		message: String
		data: profile
	}

	type docMutationResponse implements mutationResponse {
		code: String!
		success: Boolean!
		message: String!
		data: bulkResponse
	}

	type bulkResponse {
		n: Int
		nModified: Int
		ok: Int
	}

	type contact {
		name: String!
		phoneNumber: String!
		relation: String
	}

	input contactInput {
		name: String!
		phoneNumber: String!
		relation: String

	}

	enum familyStatus {
		single
		married
		divorced
		widowed
		relationship
		engaged
	}

	type homeAddress {
		country: String
		city: String
		street: String
		houseNumber: String
	}

	input homeAddressInput {
		country: String
		city: String!
		street: String!
		houseNumber: String!
	}

	type device {
		_id: String!
		deviceName: String!
		fcmId: String!
	}

	type activation {
		isActive: Boolean!
		code: String
	}
	input activationInput {
		isActive: Boolean!
	}
	
	type appify {
		installed: Boolean!
		lastUsed: String
	}
        
	type profile {
		_id: String!
		firstName: String
		lastName: String
		dateOfBirth: String
		email: String
		gender: String
		familyStatus: familyStatus
		groups: [group]
		phoneNumber: String!
		homeAddress: homeAddress
		emergencyContacts: [contact]
		medicalReport: medicalReport!
		lockScreenNotification: Boolean!
		devices: [device]
		activation: activation
		updatedAt: String
		createdAt: String
		lastLogin: String
        lastActivity: String
		appify: appify
	}

	input profileInput {
		firstName: String
		lastName: String
		dateOfBirth: String
		email: String
		gender: String
		familyStatus: familyStatus
		phoneNumber: String
		homeAddress: homeAddressInput
		emergencyContacts: [contactInput]
		lockScreenNotification: Boolean
	}

	extend type Query {
		myProfile: profile! @requireAuth
		getProfile(id: String!) : profile @requireAuth
		getProfileByPhoneNumber(phoneNumber: String!) : profile @requireAuth
		getProfiles(ids: [String!]!) : [profile] @requireAuth
	}

	extend type Mutation {
		updateProfile(id: String!,data: profileInput!) : profile! @requireAuth
		updateProfiles(ids: [String!]!, data: profileInput) : docMutationResponse! @requireAuth
		updateFamilyMember(id: String!, data: profileInput!): userMutationResponse! @requireAuth
		deleteProfile(id: String!) : docMutationResponse! @requireAuth
		deleteProfiles(ids: [String!]!) : docMutationResponse! @requireAuth
	}
`;

