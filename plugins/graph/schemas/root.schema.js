module.exports = `
	directive @requireAuth on FIELD_DEFINITION
	directive @wpAuth(role: [String]) on FIELD_DEFINITION
`;
