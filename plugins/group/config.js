module.exports = {
  MONGO_DB_URI: process.env.MONGO_DB_URI,
  MONGO_LOST_CONNECTION_RETRIES_ALERT:
    process.env.MONGO_LOST_CONNECTION_RETRIES_ALERT,
  RABBIT_MQ_URL: process.env.RABBIT_MQ_URL,
  PORT: process.env.PORT,
  DEBUG: process.env.DEBUG,
  NODE_ENV: process.env.NODE_ENV,
  alert: {
    url: process.env.ALERT_MANAGER_URL,
    username: process.env.ALERT_MANAGER_USER,
    password: process.env.ALERT_MANAGER_PASSWORD
  }
};

