const GroupModel = require("../models/group");
const UserModel = require("../../profile/models/user");
const mongoose = require("mongoose");
const createModel = async (req, res, next) => {
  const { id } = req.body;
  return new Promise((resolve, reject) => {
    GroupModel.create(
      {
        _id: new mongoose.Types.ObjectId(),
        label: "family",
        members: [{ memberId: id, role: "Owner", approved: true }],
      },
      (err, item) => {
        if (err) reject(err);
        resolve(item);
      }
    );
  });
};

const getModelById = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const modelId = req.params.id;
    const Model = await GroupModel.findById(modelId)
      .populate("memberId")
      .exec((err, item) => {
        if (err) reject(err);
        resolve(item);
      });
  });
};

const getModels = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    console.log(req.body.ids);
    const modelIds = req.body.ids;
    GroupModel.find({ _id: { $in: modelIds } })
      .populate("memberId")
      .exec(function (err, item) {
        if (err) reject(err);
        resolve(item);
      });
  });
};

const updateModel = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const { id } = req.params;
    const { data } = req.body;
    const { members, ...rest } = data;
    GroupModel.findOneAndUpdate(
      { _id: id },
      { $set: { rest }, $push: { members } },
      { upsert: false, new: true }
    ).exec(function (err, item) {
      if (err) reject(err);
      resolve(item);
    });
  });
};
const updateModels = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const { ids, data } = req.body;
    GroupModel.updateMany({ _id: { $in: ids } }, data).exec(function (
      err,
      result
    ) {
      if (err) reject(err);
      resolve(result);
    });
  });
};
const deleteModel = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const { id } = req.params;
    GroupModel.deleteOne({ _id: id }).exec(function (err, item) {
      if (err) reject(err);
      resolve(item);
    });
  });
};
const deleteModels = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const { ids } = req.body;
    GroupModel.deleteMany({ _id: { $in: ids } }).exec(function (err, result) {
      if (err) reject(err);
      resolve(result);
    });
  });
};

const addMemberToGroup = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const { id } = req.params;
    const { memberId, role, approved } = req.body.data;
    GroupModel.findOneAndUpdate(
      { _id: id },
      { $push: { members: { memberId, role, approved } } }
    ).exec(function (err, result) {
      if (err) reject(err);
      resolve(result);
    });
  });
};
const acceptGroup = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const { id } = req.params;
    const { memberId } = req.body;
    GroupModel.findOneAndUpdate(
      { _id: id, "members.memberId": memberId },
      { $set: { "members.approved": approved } }
    ).exec(function (err, result) {
      if (err) reject(err);
      resolve(result);
    });
  });
};

const groupAuthorization = async (req, res, next) => {
  const { groupId, memberId } = req.body;
  return new Promise(async (resolve, reject) => {
    GroupModel.find(
      { _id: groupId, "members.memberId": memberId },
      { members: { $elemMatch: { memberId: memberId } } }
    ).exec((err, result) => {
      resolve(result[0].members[0].role);
    });
  });
};

const middlewareProxy = (func) => async (req, res, next) => {
  try {
    return res.json({
      code: 200,
      message: "ok",
      success: true,
      data: await func(req, res, next),
    });
  } catch (error) {
    console.error(error);
    return res.json({ code: 500, message: error, success: false, data: null });
  }
};
module.exports = {
  createModel: middlewareProxy(createModel),
  getModelById: middlewareProxy(getModelById),
  getModels: middlewareProxy(getModels),
  updateModel: middlewareProxy(updateModel),
  updateModels: middlewareProxy(updateModels),
  deleteModel: middlewareProxy(deleteModel),
  deleteModels: middlewareProxy(deleteModels),
  addMemberToGroup: middlewareProxy(addMemberToGroup),
  acceptGroup: middlewareProxy(acceptGroup),
  groupAuthorization: middlewareProxy(groupAuthorization),
};
