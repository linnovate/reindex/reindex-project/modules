const mongoose = require("mongoose");
const { MONGO_DB_URI } = require("../config");
module.exports = () => {
  const connect = () => {
    mongoose.Promise = global.Promise;

    mongoose.connect(MONGO_DB_URI, {
      keepAlive: true,
      reconnectTries: Number.MAX_VALUE,
      useNewUrlParser: true
    });
    mongoose.set("useCreateIndex", true);
    mongoose.set("useFindAndModify", false);
  };
  connect();

  var db = mongoose.connection;
  db.on("error", error => console.error(error));
  db.on("open", () => {
    console.log("MongoDB Connection was successful");
  });
};
