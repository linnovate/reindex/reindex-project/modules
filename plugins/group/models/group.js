const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const GroupSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  label: { type: String, required: true, default: "family" },
  members: [
    {
      memberId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
      role: { type: String, default: "Member" },
      approved: { type: Boolean, default: false },
    },
  ],
  createdDate: { type: Date, default: Date.now() },
});

GroupSchema.plugin(mongoosePaginate);
const Group = mongoose.model("Group", GroupSchema);

module.exports = Group;
