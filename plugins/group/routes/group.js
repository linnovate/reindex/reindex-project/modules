const controller = require("../controllers/group.controller");
const express = require("express");
const router = express.Router();

router.post("/new", controller.createModel);
router.get("/:id", controller.getModelById);
router.post("/", controller.getModels);
router.put("/:id", controller.updateModel);
router.put("/:id/add", controller.addMemberToGroup);
router.post("/:id/accpet", controller.acceptGroup);
router.put("/", controller.updateModels);
router.delete("/:id", controller.deleteModel);
router.delete("/", controller.deleteModels);
router.post("/auth", controller.groupAuthorization);
module.exports = router;
