const express = require("express");
const router = express.Router();
const initMongo = require("../middleware/mongo");
initMongo();
router.use("/api/group", require("./group"));
/*
 * Handle 404 error
 */

router.use("*", (req, res) => {
  res.status(404).json({
    errors: {
      msg: "URL_NOT_FOUND"
    }
  });
});
module.exports = router;
