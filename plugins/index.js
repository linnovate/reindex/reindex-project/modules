if (process.env.SERVICE_NAME == "graph") {
  //Schemas
  const rootSchema = require("./graph/schemas/root.schema");
  const ProfileSchema = require("./graph/schemas/profile.schema");
  const ModelSchema = require("./graph/schemas/model.schema");
  const AuthenticationSchema = require("./graph/schemas/authentication.schema");
  const groupSchema = require("./graph/schemas/group.schema");
  const inviteSchmea = require("./graph/schemas/invite.schema");
  const elasticSchema = require("./graph/schemas/elastic.schema");
  const NotificationSchema = require("./graph/schemas/notification.schema");
  //Resolvers
  const rootResolver = require("./graph/resolvers/root.resolver");
  const ProfileResolver = require("./graph/resolvers/profile.resolver");
  const ModelResolver = require("./graph/resolvers/model.resolver");
  const AuthenticationResolver = require("./graph/resolvers/authentication.resolver");
  const groupResolver = require("./graph/resolvers/group.resolver");
  const inviteResolver = require("./graph/resolvers/invite.resolver");
  const elasticResolver = require("./graph/resolvers/elastic.resolver");
  const NotificationResolver = require("./graph/resolvers/notification.resolver");
  //Directives
  const {
    requireAuthDirective,
  } = require("./graph/directives/requireAuthDirective");
  const {
    groupAccessDirective,
  } = require("./graph/directives/groupAccessDirective");
  const {
    wordpressAuthDirective,
  } = require("./graph/directives/wordpressAuthDirective");
  //Connectors
  const { ProfileApi } = require("./graph/connectors/profileApi");
  const { ModelApi } = require("./graph/connectors/modelApi");
  const { GroupApi } = require("./graph/connectors/groupApi");
  const { ConsumerApi } = require("./graph/connectors/consumerApi");
  const { ElasticsearchApi } = require("./graph/connectors/elasticsearchApi");
  const { NotificationApi } = require("./graph/connectors/notificationApi");
  const { WordpressApi } = require("./graph/connectors/wordpressApi");
  module.exports = {
    graph: {
      dataSources: {
        profileApi: new ProfileApi(process.env.PROFILE_URI),
        modelApi: new ModelApi(process.env.MODEL_URI),
        groupApi: new GroupApi(process.env.GROUP_URI),
        consumerApi: new ConsumerApi(process.env.CONSUMER_URI),
        elasticsearchApi: new ElasticsearchApi(
          process.env.ELASTIC_SEARCH_URL,
          process.env.ELASTIC_SEARCH_INDEX
        ),
        notificationApi: new NotificationApi(process.env.NOTIFICATIONS_URI),
        wordpressApi: new WordpressApi(process.env.WORDPRESS_URI),
      },
      connectors: {},
      extensionsRootTypeDef: [rootSchema],
      extensionsTypeDef: [
        ProfileSchema,
        ModelSchema,
        AuthenticationSchema,
        groupSchema,
        inviteSchmea,
        elasticSchema,
        NotificationSchema,
        inviteSchmea,
      ],
      extensionsResolvers: [
        ProfileResolver,
        ModelResolver,
        AuthenticationResolver,
        groupResolver,
        inviteResolver,
        elasticResolver,
        NotificationResolver,
        rootResolver,
      ],
      extensionsDirectives: [
        {
          requireAuth: requireAuthDirective,
          groupAccess: groupAccessDirective,
          wordpressAuth: wordpressAuthDirective,
        },
      ],
    },
  };
}
if (process.env.SERVICE_NAME == "profile") {
  const ProfileRouter = require("./profile/routes/index");
  module.exports = {
    model: {
      routes: [
        {
          serviceName: "profile",
          router: ProfileRouter,
        },
      ],
    },
  };
}
if (process.env.SERVICE_NAME == "model") {
  const ModelRouter = require("./model/routes/index");
  module.exports = {
    model: {
      routes: [
        {
          serviceName: "model",
          router: ModelRouter,
        },
      ],
    },
  };
}
// {
//   QueueName: "test",
//   Controller: () => {
//     console.log(test);
//   },
//   Settings: { noAck: 1 }
// }
if (process.env.SERVICE_NAME == "notifications") {
  const NotificationsController = require("./notifications/controller");
  module.exports = {
    consumer: {
      routes: [
        {
          serviceName: "notifications",
          name: "/api/notification",
          queue: "sendNotification",
        },
        {
          serviceName: "notifications",
          name: "/api/binding",
          queue: "createBinding",
        },
      ],
      queues: [
        {
          name: "sendNotification",
          controller: NotificationsController.sendNotification,
        },
        {
          name: "createBinding",
          controller: NotificationsController.createBinding,
        },
      ],
    },
  };
}

if (process.env.SERVICE_NAME == "consumer") {
  const ConsumerController = require("./consumer/controller");
  module.exports = {
    consumer: {
      routes: [
        {
          serviceName: "consumer",
          name: "/api/elastic/index",
          queue: "elasticIndex",
        },
        {
          serviceName: "consumer",
          name: "/api/fallbackRequest",
          queue: "fallbackRequest",
        },
      ],
      queues: [
        {
          name: "elasticIndex",
          controller: ConsumerController.elasticIndex,
        },
        {
          name: "fallbackRequest",
          controller: ConsumerController.fallbackRequest,
        },
      ],
    },
  };
}
if (process.env.SERVICE_NAME == "group") {
  const GroupRouter = require("./group/routes/index");
  module.exports = {
    model: {
      routes: [
        {
          serviceName: "group",
          router: GroupRouter,
        },
      ],
    },
  };
}
