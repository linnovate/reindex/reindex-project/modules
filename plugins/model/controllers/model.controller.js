const MedicalReport = require("../models/model");
const mongoose = require("mongoose");
const createModel = async (req, res, next) => {
  return new Promise((resolve, reject) => {
    MedicalReport.create(
      { _id: new mongoose.Types.ObjectId() },
      (err, item) => {
        if (err) reject(err);
        resolve(item);
      }
    );
  });
};

const getModelById = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const modelId = req.params.id;
    const Model = await MedicalReport.findById(modelId, (err, item) => {
      if (err) reject(err);
      resolve(item);
    });
  });
};

const getModels = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const modelIds = req.body.ids;
    MedicalReport.find({ _id: { $in: modelIds } }).exec(function(err, item) {
      if (err) reject(err);
      resolve(item);
    });
  });
};

const updateModel = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const { id } = req.params;
    const { data } = req.body;
    MedicalReport.updateOne({ _id: id }, data).exec(function(err, item) {
      if (err) reject(err);
      resolve(item);
    });
  });
};
const updateModels = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const { ids, data } = req.body;
    MedicalReport.updateMany({ _id: { $in: ids } }, data).exec(function(
      err,
      result
    ) {
      if (err) reject(err);
      resolve(result);
    });
  });
};
const deleteModel = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const { id } = req.params;
    MedicalReport.deleteOne({ _id: id }).exec(function(err, item) {
      if (err) reject(err);
      resolve(item);
    });
  });
};
const deleteModels = async (req, res, next) => {
  return new Promise(async (resolve, reject) => {
    const { ids } = req.body;
    MedicalReport.deleteMany({ _id: { $in: ids } }).exec(function(err, result) {
      if (err) reject(err);
      resolve(result);
    });
  });
};

const middlewareProxy = func => async (req, res, next) => {
  try {
    return res.json({
      code: 200,
      message: "ok",
      success: true,
      data: await func(req, res, next)
    });
  } catch (error) {
    console.error(error);
    return res.json({ code: 500, message: error, success: false, data: null });
  }
};
module.exports = {
  createModel: middlewareProxy(createModel),
  getModelById: middlewareProxy(getModelById),
  getModels: middlewareProxy(getModels),
  updateModel: middlewareProxy(updateModel),
  updateModels: middlewareProxy(updateModels),
  deleteModel: middlewareProxy(deleteModel),
  deleteModels: middlewareProxy(deleteModels)
};
