const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");

const ModelSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  overallMedicalCondition: { type: String, default: null },
  significantMedicalEvents: [
    {
      date: { type: String, default: null },
      description: { type: String, default: null }
    }
  ],
  sensitivities: [
    {
      cause: { type: String, default: null },
      effect: { type: String, default: null },
      severity: { type: Number, default: null }
    }
  ],
  regularMedications: [
    {
      name: { type: String, default: null },
      frequency: {
        dateFormat: { type: String, default: null },
        interval: { type: Number, default: null }
      }
    }
  ],
  bloodType: { type: String, default: null }
});

ModelSchema.plugin(mongoosePaginate);
const Model = mongoose.model("Model", ModelSchema);

module.exports = Model;
