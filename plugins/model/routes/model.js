const controller = require("../controllers/model.controller");
const express = require("express");
const router = express.Router();

router.post("/", controller.createModel);
router.get("/:id", controller.getModelById);
router.get("/", controller.getModels);
router.put("/:id", controller.updateModel);
router.put("/", controller.updateModels);
router.delete("/:id", controller.deleteModel);
router.delete("/", controller.deleteModels);
module.exports = router;
