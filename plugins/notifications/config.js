module.exports = {
    transport: {
        twilio: {
            api_key: process.env.TWILIO_API_KEY,
            api_secret: process.env.TWILIO_API_SECRET,
            account_sid: process.env.TWILIO_ACCOUNT_SID,
            notification_service_sid: process.env.TWILIO_NOTIFICATION_SERVICE_SID
        }
    }
};