const { notificationService } = require("../transport/twilio");
const _ = require("lodash");

const validatePushRequest = body => {
  return new Promise((resolve, reject) => {
    let data = { ...body };
    if (_.isEmpty(data)) {
      return reject("Missing request body");
    }

    if (_.isEmpty(data.identity)) {
      return reject("There must exist identity");
    }

    if (
      _.isEmpty(data.tag) ||
      data.tag === null ||
      data.tag.length == 0 ||
      data.tag[0] == null
    ) {
      data.tag = [
        "preferred device",
        process.env.TWILIO_GLOBAL_TAG || "global"
      ];
    } else
      data.tag = [
        ...data.tag,
        "preferred device",
        process.env.TWILIO_GLOBAL_TAG || "global"
      ];

    // if (_.isEmpty(body.content)) {
    //     return next(new errors.BadRequestError('The request body must contain a non-empty \'content\' object'));
    // }

    return resolve(data);
  });
};
module.exports = async function(body, callback) {
  try {
    const data = await validatePushRequest(body);
    console.log(JSON.stringify(data));
    return notificationService.bindings
      .create(data)
      .then(binding => {
        console.log(binding);
        return callback({ message: "Binding created!" });
      })
      .catch(error => {
        return callback({ error: true, message: error });
      });
  } catch (error) {
    return callback({ error: true, message: error });
  }
};

// {
//   endpoint: 'XXXXXXXXXXXXXXX',
//   tag: ['preferred device'],
//   identity: '00000001',
//   bindingType: 'fcm',
//   address: 'fcm_device_token'
// }
