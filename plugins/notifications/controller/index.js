const sendNotification = require("./sendNotification");
const createBinding = require("./createBinding");

module.exports.sendNotification = function(ch, msg) {
  sendNotification(JSON.parse(msg.content.toString()), (error, output) => {
    const reply = error || output;
    ch.sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify(reply)), {
      correlationId: msg.properties.correlationId
    });
    ch.ack(msg);
  });
};

module.exports.createBinding = function(ch, msg) {
  createBinding(JSON.parse(msg.content.toString()), (error, output) => {
    const reply = error || output;
    ch.sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify(reply)), {
      correlationId: msg.properties.correlationId
    });
    ch.ack(msg);
  });
};
