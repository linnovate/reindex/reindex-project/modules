const { notificationService } = require("../transport/twilio");

module.exports = function(body, callback) {
  // Send a notification
  return notificationService.notifications
    .create(body)
    .then(message => {
      console.log("message", message);
      return callback(null, { message: "Successful sending notification" });
    })
    .catch(error => {
      console.log(error);
      return callback(error, null);
    });
};
