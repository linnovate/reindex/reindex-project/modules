const config = require('../config');
const Twilio = require('twilio');

// Twilio Library
const messenger = new Twilio(
    config.transport.twilio.api_key,
    config.transport.twilio.api_secret, { accountSid: config.transport.twilio.account_sid }
);
// Get a reference to the user notification service instance
const notificationService = messenger.notify.services(
    config.transport.twilio.notification_service_sid
);

module.exports.messenger = messenger;
module.exports.notificationService = notificationService