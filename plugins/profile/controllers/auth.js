const User = require("../models/user");
const Device = require("../models/device");
const Appify = require("../models/appify");

const jwt = require("jsonwebtoken");
const config = require("../config");
const mongoose = require("mongoose");

const checkExistingUser = async (searchParam) => {
  console.debug("checkExistingUser");
  return new Promise((resolve, reject) => {
    User.findOne({
      ...searchParam,
    })
      .populate("devices")
      .populate("appify")
      .exec((err, item) => {
        if (!item) resolve(false);
        else resolve(item);
        if (err) {
          reject({
            code: 422,
            message: err.message,
          });
        }
      });
  });
};

const registerUser = async (data) => {
  if (!data) return { code: 401, message: "No Register data" };
  return new Promise((resolve, reject) => {
    const { device, appify, ...rest } = data;
    const user = new User({
      _id: new mongoose.Types.ObjectId(),
      ...rest,
    });
    user.save((err, item) => {
      if (err) {
        reject({
          code: 422,
          message: err.message,
        });
      }
      resolve(item);
    });
  });
};
const registerDevice = async (data) => {
  if (!data.device || !data.device.fcmId || !data.device.deviceName) return;
  return new Promise((resolve, reject) => {
    const device = new Device({
      _id: new mongoose.Types.ObjectId(),
      name: data.device.deviceName,
      address: data.device.fcmId,
    });
    device.save((err, item) => {
      if (err) {
        reject({
          code: 422,
          message: err.message,
        });
      }
      resolve(item);
    });
  });
};

const registerAppify = async (data) => {
  let installed = data.appify ? (data.appify.installed ? true : false) : false;
  return new Promise((resolve, reject) => {
    const appify = new Appify({
      _id: new mongoose.Types.ObjectId(),
      installed: installed,
    });
    installed ? (appify.lastUsed = Date.now()) : null;
    appify.save((err, item) => {
      if (err) reject(err);
      resolve(item);
    });
  });
};
const generateToken = async (id) => {
  const obj = {
    _id: id,
  };
  return jwt.sign(obj, config.JWT_SECRET, {
    expiresIn: config.JWT_EXPIRES_IN,
  });
};

exports.signup = async (req, res) => {
  console.log("signup");
  try {
    const data = req.body.data;
    if (!data) {
      console.debug("no signup data passed");
      return res.status(401).json({
        code: 401,
        message: "No register data sent",
        data: null,
        token: null,
      });
    }
    const user = await checkExistingUser({ phoneNumber: data.phoneNumber });
    if (!user) {
      console.debug("signup found no user, creating");
      const user = await registerUser(data);
      const device = await registerDevice(data);
      const appify = await registerAppify(data);
      if (appify) user.appify = appify._id;
      if (device) user.devices.push(device._id);
      await user.save();
      let token = await generateToken(user._id);
      res.cookie("jwt", token);
      User.findById(user._id).exec((err, item) => {
        res.status(201).json({
          code: 201,
          token: token,
          message: "Registered Successfully",
          data: item,
        });
      });
    } else {
      console.debug("signup found user, user already exists");
      res.status(403).json({
        code: 403,
        token: null,
        message: "User already exists",
        data: null,
      });
    }
  } catch (error) {
    console.error(error);
  }
};

exports.login = async (req, res) => {
  try {
    const data = req.body.data;
    const user = await checkExistingUser({ phoneNumber: data.phoneNumber });
    const password = data.password;
    if (user) {
      await user.comparePassword(password, async (err, result) => {
        if (err) return false;

        if (result) {
          // Passwords match
          const device = await registerDevice(data);
          if (device) user.devices.push(device._id);
          if (data.appify && data.appify.installed)
            user.appify.updateLastUsed(true);
          await user.save();
          const token = await generateToken(user._id);
          res.cookie("jwt", token);
          res.status(200).json({
            code: 200,
            token: token,
            message: "logged in successfully",
            data: user,
          });
        } else {
          // Passwords don't match
          res.status(400).json({
            code: 400,
            token: null,
            error: "Username or Password doesn't match",
            data: null,
          });
        }
      });
    } else {
      res.status(400).json({
        code: 400,
        token: null,
        error: "Username or Password doesn't match",
        data: null,
      });
    }
  } catch (error) {
    res.status(502).json({
      token: null,
      message: "something went wrong while trying to login",
    });
    console.error(error);
  }
};

exports.join = async (req, res, next) => {
  try {
    const { id, data } = req.body;
    const user = await checkExistingUser({ _id: id });
    if (!user) {
      return res.status(400).json({
        code: 400,
        token: null,
        error: "user doesnt exists",
        data: null,
      });
    } else {
      if (data.code != user.activation.activationCode)
        return res.status(401).json({
          code: 401,
          message: "activation code isn't right",
          token: null,
          data: null,
        });
      if (user.activation.isActive) {
        return res.status(401).json({
          code: 401,
          message: "user is already activated please login",
          token: null,
          data: null,
        });
      } else {
        user.activation.isActive = true;
      }
      const device = await registerDevice(data);
      if (device) user.devices.push(device._id);
      if (data.appify && data.appify.installed)
        user.appify.updateLastUsed(true);
      delete data.appify;
      delete data.device;
      delete data.code;
      Object.keys(data).forEach((key) => {
        console.debug(`${key} : ${data[key]}`);
        user[key] = data[key];
      });

      console.debug("user pre saving:" + JSON.stringify(user));
      await user.save();
      const token = await generateToken(user._id);
      res.cookie("jwt", token);
      return res.status(200).json({
        code: 200,
        message: "joined successfully",
        token: token,
        data: user,
      });
    }
  } catch (err) {
    res.status(502).json({
      token: null,
      message: "something went wrong while trying to login",
    });
    console.error(error);
  }
};
