const User = require("../models/user");
const Device = require("../models/device");
const Appify = require("../models/appify");
const Model = require("../../model/models/model");
const Group = require("../../group/models/group");

const deleteProfile = async (req, res, next) => {
  const { id } = req.params;
  return new Promise((resolve, reject) => {
    User.deleteOne(
      {
        _id: id,
      },
      function (err, res) {
        if (!err) {
          resolve(res);
        }
        reject(err);
      }
    );
  });
};
const deleteProfiles = async (req, res, next) => {
  const { ids } = req.params;
  return new Promise((resolve, reject) => {
    User.deleteMany(
      {
        _id: {
          $in: ids,
        },
      },
      function (err, res) {
        if (!err) {
          resolve(res);
        }
        reject(err);
      }
    );
  });
};
const getProfile = async (req, res, next) => {
  const { id, phoneNumber } = req.query;
  const fetch = id ? { _id: id } : { phoneNumber };
  return new Promise((resolve, reject) => {
    User.findOne(fetch)
      .populate("groups")
      .populate("medicalReport")
      .populate("devices")
      .populate("appify")
      .exec((err, item) => {
        if (err) {
          reject(err);
        }
        resolve(item);
      });
  });
};

const getProfiles = async (req, res, next) => {
  const { ids } = req.body;
  const query = ids && ids.length ? { _id: { $in: ids } } : {};
  return new Promise(async (resolve, reject) => {
    User.find(query)
      .populate("groups")
      .populate("medicalReport")
      .populate("devices")
      .exec(function (err, results) {
        if (err) {
          reject(err);
        }
        resolve(results);
      });
  });
};

const updateProfile = async (req, res, next) => {
  const { id, data } = req.body;

  return new Promise((resolve, reject) => {
    User.findByIdAndUpdate(id, data, { new: true })
      .populate("groups")
      .populate("medicalReport")
      .populate("devices")
      .exec(function (err, doc) {
        if (err) reject(err);

        resolve(doc);
      });
  });
};

const updateProfiles = async (req, res, next) => {
  const { ids, data } = req.body;
  return new Promise((resolve, reject) => {
    User.updateMany(
      {
        _id: {
          $in: ids,
        },
      },
      {
        $set: data,
      },
      {
        multi: true,
      }
    )
      .populate("groups")
      .populate("medicalReport")
      .populate("devices")
      .exec(function (err, doc) {
        if (err) {
          reject(err);
        }
        return resolve(doc);
      });
  });
};

const middlewareProxy = (func) => async (req, res, next) => {
  try {
    return res.json({
      code: 200,
      message: "ok",
      data: await func(req, res, next),
    });
  } catch (error) {
    console.error(error);
    return res.json({ code: 500, message: error, data: null });
  }
};
module.exports = {
  getProfile: middlewareProxy(getProfile),
  getProfiles: middlewareProxy(getProfiles),
  updateProfile: middlewareProxy(updateProfile),
  updateProfiles: middlewareProxy(updateProfiles),
  deleteProfile: middlewareProxy(deleteProfile),
  deleteProfiles: middlewareProxy(deleteProfiles),
};
