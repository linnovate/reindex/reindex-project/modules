const passport = require("passport");
const User = require("../models/user");
const JwtStrategy = require("passport-jwt").Strategy;
const BearerStrategy = require("passport-http-bearer");
const Config = require("../config.js");

// Extracting the JWT Token from the headers
const jwtExtractor = (req) => {
  let token = null;
  if (req.cookies.token) {
    token = req.cookies.token.replace(" ", "");
  } else if (req.headers.authorization) {
    token = req.headers.authorization.replace("Bearer ", "").replace(" ", "");
  } else if (req.body.token) {
    token = req.body.token.replace(" ", "");
  } else if (req.query.token) {
    token = req.query.token.replace(" ", "");
  }
  return token;
};

// JWT Object for verfication
const jwtOptions = {
  jwtFromRequest: jwtExtractor,
  secretOrKey: Config.JWT_SECRET,
};

// Validate the JWT
const jwtLogin = new JwtStrategy(jwtOptions, (payload, done) => {
  User.findByIdAndUpdate(payload._id, { lastLogin: Date.now() })
    .populate("groups")
    .populate("medicalReport")
    .populate("devices")
    .populate("appify")
    .exec((err, user) => {
      if (err) {
        return done(null, false);
      }
      return !user ? done(null, false) : done(null, user);
    });
});

// Define passport strategies
passport.use(jwtLogin);
passport.use(
  new BearerStrategy(function (token, done) {
    if (token === Config.ADMIN_SECRET_TOKEN) {
      return done(null, true);
    }
    return done(null, false);
  })
);
