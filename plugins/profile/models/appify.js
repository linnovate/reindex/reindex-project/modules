const mongoose = require("mongoose");

const AppifySchema = new mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    installed: { type: Boolean, default: false },
    lastUsed: Date,
  },
  {
    timestamps: true,
  }
);

AppifySchema.methods.updateLastUsed = async function (installed) {
  let appify = this;
  return new Promise((resolve, reject) => {
    appify.installed = installed;
    appify.lastUsed = Date.now();
    appify.save((err, item) => {
      if (err) reject(err);
      resolve(item);
    });
  });
};

const Appify = mongoose.model("Appify", AppifySchema);

module.exports = Appify;
