const mongoose = require("mongoose");

const DeviceSchema = new mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    address: String
  },
  {
    timestamps: true
  }
);

const Device = mongoose.model("Device", DeviceSchema);

module.exports = Device;

