const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const mongoosePaginate = require("mongoose-paginate-v2");
const { PASSWORD_HASHING_ROUNDS } = require("../config");

const generateActivationCode = (length = 6) => {
  var result = "";
  var characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result.toUpperCase();
};

const UserSchema = new mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    firstName: String,
    lastName: String,
    password: {
      type: String,
      required: false,
    },
    email: { type: String, required: false },
    phoneNumber: { type: String, required: true, unique: true },
    familyStatus: { type: String, required: false, default: "single" },
    gender: { type: String, required: false },
    dateOfBirth: { type: String },
    homeAddress: {
      country: { type: String },
      city: { type: String },
      street: { type: String },
      houseNumber: { type: String },
    },
    medicalReport: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Model",
    },
    emergencyContacts: [
      {
        name: { type: String, default: null },
        phoneNumber: { type: String, default: null },
        relation: { type: String, default: null },
      },
    ],
    lockScreenNotification: { type: Boolean, default: true },
    groups: [{ type: mongoose.Schema.Types.ObjectId, ref: "Group" }],
    lastLogin: { type: Date, default: Date.now() },
    lastActivity: { type: Date, default: Date.now() },
    activation: {
      isActive: { type: Boolean, default: false },
      activationCode: { type: String, default: () => generateActivationCode() },
    },
    devices: [{ type: mongoose.Schema.Types.ObjectId, ref: "Device" }],
    appify: { type: mongoose.Schema.Types.ObjectId, ref: "Appify" },
  },
  { timestamps: true }
);

UserSchema.pre("save", function (next) {
  var user = this;
  if (!user.password) return next();

  // only hash the password if it has been modified (or is new)
  if (!user.isModified("password")) return next();

  // generate a salt
  bcrypt.genSalt(parseInt(PASSWORD_HASHING_ROUNDS), function (err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);

      console.debug("Pre user save, hashing password");
      // override the cleartext password with the hashed one
      user.password = hash;
      next();
    });
  });
});

UserSchema.pre("updateOne", function (next) {
  var user = this;

  if (user.isModified("password")) {
    UserSchema.hashPassword(user.password, (err, result) => {
      user.password = result;
      next();
    });
  }
});

UserSchema.statics.hashPassword = async function (candidatePassword, cb) {
  bcrypt.genSalt(parseInt(PASSWORD_HASHING_ROUNDS), function (err, salt) {
    if (err) return cb(err);

    // hash the password using our new salt
    bcrypt.hash(candidatePassword, salt, function (err, hash) {
      if (err) return cb(err);
      // override the cleartext password with the hashed one
      cb(null, hash);
    });
  });
};

UserSchema.methods.comparePassword = async function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

UserSchema.methods.removePasswordReturnUser = () => {
  let user = this.toObject();
  delete user.hashed_password;
  return user;
};

UserSchema.plugin(mongoosePaginate);
const User = mongoose.model("User", UserSchema);

module.exports = User;
