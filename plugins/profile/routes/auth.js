const controller = require("../controllers/auth");
const express = require("express");
const router = express.Router();
require("../middleware/passport");
const passport = require("passport");
const requireAuth = passport.authenticate("jwt", {
  session: false
});

router.post("/register", controller.signup);
router.post("/login", controller.login);
router.post("/join", controller.join);

router.post("/checkAuthentication", requireAuth, (req, res) => {
  res.json({ code: 200, data: req.user });
});

module.exports = router;
