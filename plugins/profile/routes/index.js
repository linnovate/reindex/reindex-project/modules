const express = require("express");
const router = express.Router();
const initMongo = require("../middleware/mongo");
initMongo();
router.use("/auth", require("./auth"));
router.use("/api/profile", require("./profile"));
/*
 * Handle 404 error
 */

router.use("*", (req, res) => {
  res.status(404).json({
    errors: {
      msg: "URL_NOT_FOUND"
    }
  });
});
module.exports = router;
