const controller = require("../controllers/profile");
const express = require("express");
const router = express.Router();
require("../middleware/passport");

// Get One Profile
router.get("/", controller.getProfile);
// Get Many Profiles
router.post("/", controller.getProfiles);
// Update one profile
router.put("/", controller.updateProfile);
// Update many profiles
router.put("/bulk", controller.updateProfiles);
// Delete one profile
router.delete("/", controller.deleteProfile);
// Delete many profiles
router.delete("/bulk", controller.deleteProfiles);

module.exports = router;
